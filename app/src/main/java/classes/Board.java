package classes;

import graphics.GameFrame;

import java.util.ArrayList;

/**
 * Created by Anvar on 12.12.15.
 */
public class Board {

    public enum Direction {
        UP(0), RIGHT(1), DOWN(2), LEFT(3);

        private int value;

        Direction(int value) {
            this.value = value;
        }

        public static Direction getDiretionFromValue(int value){

            switch (value){
                case 0:
                    return UP;
                case 1:
                    return RIGHT;
                case 2:
                    return DOWN;
                case 3:
                    return LEFT;
                default:
                    return UP;
            }

        }

        public int getValue() {
            return value;
        }

        public int getX(){
            if (this.equals(RIGHT))
                return 1;
            else if (this.equals(LEFT))
                return -1;
            else
                return 0;
        }

        public int getY(){
            if (this.equals(DOWN))
                return 1;
            else if (this.equals(UP))
                return -1;
            else
                return 0;
        }
    }

    public enum CellType {
        EMPTY(0), UNBREAKABLE(1), BREAKABLE(2), PLAYER(3), UNDEFINED(4), UNDEFINED_WALL(5), BREAKED_WALL(6);

        private int value;

        CellType(int value) {
            this.value = value;
        }

        public static CellType getCellTypeFromValue(int value){

            switch (value){
                case 0:
                    return EMPTY;
                case 1:
                    return UNBREAKABLE;
                case 2:
                    return BREAKABLE;
                case 3:
                    return PLAYER;
                case 4:
                    return UNDEFINED;
                case 5:
                    return UNDEFINED_WALL;
                case 6:
                    return BREAKED_WALL;
                default:
                    return EMPTY;
            }

        }

        public int getValue() {
            return value;
        }
    }

    public static final int BOARD_SIZE = 11;
    public static final int BOARD_VALID_SIZE = 5;

    private ArrayList<CellType[][]> fields;
    private ArrayList<Coordinates> currentPlayerPositions;
    private GameFrame gameFrame;
//    private Client client;

    public Board(GameFrame gameFrame) {
        this.gameFrame = gameFrame;
        initializeBoard();
    }

    private void initializeBoard(){
        fields = new ArrayList<>();
        currentPlayerPositions = new ArrayList<>();
        for (int k = 0; k < 2; k++) {
            this.fields.add(new CellType[BOARD_SIZE][BOARD_SIZE]);
            this.currentPlayerPositions.add(new Coordinates(BOARD_SIZE / 2, BOARD_SIZE / 2));

            for (int i = 0; i < this.fields.get(k).length; i++) {
                for (int j = 0; j < this.fields.get(k)[i].length; j++) {
                    this.fields.get(k)[i][j] = CellType.UNDEFINED;
                }
            }
            this.fields.get(k)[currentPlayerPositions.get(k).getX()][currentPlayerPositions.get(k).getY()] = CellType.EMPTY;
        }
    }

    public void setPlayerStartPosition(int playerIndex, Coordinates coordinates){
        getCurrentPlayerPositions().add(playerIndex, coordinates);
    }

    public void move(Direction direction, int playerIndex){

        gameFrame.mark(
                new Coordinates(currentPlayerPositions.get(playerIndex).getX(), currentPlayerPositions.get(playerIndex).getY()),
                playerIndex,
                fields.get(playerIndex)[currentPlayerPositions.get(playerIndex).getX()][currentPlayerPositions.get(playerIndex).getY()]);

        currentPlayerPositions.get(playerIndex).makeOffset(direction.getX(), direction.getY());

        gameFrame.mark(
                new Coordinates(currentPlayerPositions.get(playerIndex).getX(), currentPlayerPositions.get(playerIndex).getY()),
                playerIndex,
                CellType.PLAYER);

    }

    public void showWall(Direction direction, int playerIndex, CellType cellType){
        gameFrame.mark(
                new Coordinates(currentPlayerPositions.get(playerIndex).getX() + direction.getX(), currentPlayerPositions.get(playerIndex).getY() + direction.getY()),
                playerIndex,
                cellType);
    }

    public CellType getCellType(int fieldNumber, Coordinates coordinates){
        return this.fields.get(fieldNumber)[coordinates.getX()][coordinates.getY()];
    }

    public CellType getCellType(int fieldNumber, int x, int y){
        return this.fields.get(fieldNumber)[x][y];
    }

    public void setCellType(int fieldNumber, Direction direction, CellType cellType){
        System.out.println("Current player position: [x: " + getCurrentPlayerPositions().get(fieldNumber).getX() + ", y: " + getCurrentPlayerPositions().get(fieldNumber).getY() + "]");
        System.out.println("Future player position: [x: " + (currentPlayerPositions.get(fieldNumber).getX() + direction.getX()) + ", y: " + (currentPlayerPositions.get(fieldNumber).getY() + direction.getY()) + "]");
        this.fields.get(fieldNumber)[currentPlayerPositions.get(fieldNumber).getX() + direction.getX()][currentPlayerPositions.get(fieldNumber).getY() + direction.getY()] = cellType;

//        gameFrame.mark(
//                new Coordinates(currentPlayerPositions.get(fieldNumber).getX() + direction.getX(), currentPlayerPositions.get(fieldNumber).getY() + direction.getY()),
//                fieldNumber,
//                cellType);
    }

    public ArrayList<Coordinates> getCurrentPlayerPositions() {
        return currentPlayerPositions;
    }

    public void setCurrentPlayerPositions(ArrayList<Coordinates> currentPlayerPositions) {
        this.currentPlayerPositions = currentPlayerPositions;
    }
}
