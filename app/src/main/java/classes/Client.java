package classes;

import graphics.GameFrame;
import graphics.StartGame;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.*;
import java.net.Socket;

/**
 * Created by Anvar on 12.12.15.
 */
public class Client {

    public static final int PORT = 8901;
    public static final String HOST = "localhost";

    private PrintWriter output;
    private BufferedReader input;
    private GameFrame gameFrame;
    private Board myBoard;
    private StartGame startGameFrame;

    public Client(String host, int port) {

        Socket socket = null;

        try {
            socket = new Socket(host, port);

            output = new PrintWriter(socket.getOutputStream(), true);
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            System.out.println("Connection with " + socket + " is opened!");
            startGameFrame = new StartGame();

            play();
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("There is no conncetion with server!");
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("Thanks for the game!");
        }
    }

    private void play(){

        try {
            loop: while (true) {
                System.out.println("Waiting for message from server..");
                int action = Integer.parseInt(input.readLine());

                switch (action){
                    case 0:
                        startGameFrame.setVisible(false);

                        gameFrame = new GameFrame();

                        myBoard = new Board(gameFrame);

                        System.out.println("Server started the game!");
                        setKeyListeners(gameFrame);
                        break;
                    case 1:
                        int directionValue = Integer.parseInt(input.readLine());
                        Board.Direction direction = Board.Direction.getDiretionFromValue(directionValue);
                        int cellTypeValue = Integer.parseInt(input.readLine());
                        Board.CellType cellType = Board.CellType.getCellTypeFromValue(cellTypeValue);
                        int playerTurnNumber = Integer.parseInt(input.readLine());

                        if (cellType.equals(Board.CellType.BREAKED_WALL)){
                            JOptionPane.showConfirmDialog(gameFrame, ((playerTurnNumber == 0) ? "You" : "Enemy") + " just found the breaked wall!",
                                    "INFO", JOptionPane.DEFAULT_OPTION);
                        }

                        myBoard.setCellType(playerTurnNumber, direction, cellType);

                        System.out.println("move to " + direction.toString() + " and it's " + ((playerTurnNumber == 1) ? "NOT" : "") + " YOUR TURN");
                        myBoard.move(direction, playerTurnNumber);

                        break;
                    case 2:
                        int wallDirectionValue = Integer.parseInt(input.readLine());
                        Board.Direction wallDirection = Board.Direction.getDiretionFromValue(wallDirectionValue);
                        int wallCellTypeValue = Integer.parseInt(input.readLine());
                        Board.CellType wallCellType = Board.CellType.getCellTypeFromValue(wallCellTypeValue);
                        int wallPlayerTurnNumber = Integer.parseInt(input.readLine());

//                        if (wallCellType.equals(Board.CellType.BREAKED_WALL)){
//                            JOptionPane.showConfirmDialog(gameFrame, ((wallPlayerTurnNumber == 0) ? "You" : "Enemy") + " just destroyed a breakable wall and was permanently moved to its position!",
//                                    "INFO", JOptionPane.DEFAULT_OPTION);
//                        }

                        myBoard.setCellType(wallPlayerTurnNumber, wallDirection, wallCellType);

                        System.out.println("there's a wall at " + wallDirection.toString() + " and it's " + wallCellType.toString());
                        myBoard.showWall(wallDirection, wallPlayerTurnNumber, wallCellType);

                        break;
                    case 4:
                        int dialogDirectionValue = Integer.parseInt(input.readLine());

                        int result = JOptionPane.showConfirmDialog(gameFrame, "There's some obstacle in this direction. Do you want to try to destroy it? If not, you should make another move.",
                                "alert", JOptionPane.OK_CANCEL_OPTION);

//                        myBoard.setCellType(wallPlayerTurnNumber, wallDirection, wallCellType);
//                        myBoard.showWall(Board.Direction.getDiretionFromValue(dialogDirectionValue), 0, Board.CellType.UNDEFINED_WALL);

                        if (result == JOptionPane.OK_OPTION){
                            destroyRequest(Board.Direction.getDiretionFromValue(dialogDirectionValue));
                        }

                        break;
                    case 8:
                        System.out.println("You won the game!");
                        JOptionPane.showConfirmDialog(gameFrame, "YOU WON THE GAME!",
                                "INFO", JOptionPane.DEFAULT_OPTION);
                        gameFrame.dispose();
                        break loop;
                    case 9:
                        System.out.println("You lost the game!");
                        JOptionPane.showConfirmDialog(gameFrame, "YOU LOST THE GAME!",
                                "INFO", JOptionPane.DEFAULT_OPTION);
                        gameFrame.dispose();
                        break loop;
                    default:
                        System.out.println("Unexpected action from server!");
                        break loop;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException npe){
            npe.printStackTrace();
            System.out.println("Server is not responding.");
        }
    }

    public void setKeyListeners(GameFrame gameFrame){
        gameFrame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                System.out.println("SOMETHING");
                int keyCode = e.getKeyCode();
                switch( keyCode ) {
                    case KeyEvent.VK_UP:
                        moveRequest(Board.Direction.UP);
                        System.out.println("UP key pressed");
                        break;
                    case KeyEvent.VK_DOWN:
                        moveRequest(Board.Direction.DOWN);
                        System.out.println("DOWN key pressed");
                        break;
                    case KeyEvent.VK_LEFT:
                        moveRequest(Board.Direction.LEFT);
                        System.out.println("LEFT key pressed");
                        break;
                    case KeyEvent.VK_RIGHT :
                        moveRequest(Board.Direction.RIGHT);
                        System.out.println("RIGHT key pressed");
                        break;
                    case KeyEvent.VK_SPACE:
                        System.out.println("SPACE key pressed");
                        break;
                    case KeyEvent.VK_W:
                        destroyRequest(Board.Direction.UP);
                        System.out.println("W key pressed");
                        break;
                    case KeyEvent.VK_S:
                        destroyRequest(Board.Direction.DOWN);
                        System.out.println("S key pressed");
                        break;
                    case KeyEvent.VK_A:
                        destroyRequest(Board.Direction.LEFT);
                        System.out.println("A key pressed");
                        break;
                    case KeyEvent.VK_D:
                        destroyRequest(Board.Direction.RIGHT);
                        System.out.println("D key pressed");
                        break;
                    default:
                        break;
                }
            }
        });
    }

    public void moveRequest(Board.Direction direction){
        Board.CellType cellType = myBoard.getCellType(0, new Coordinates(myBoard.getCurrentPlayerPositions().get(0).getX() + direction.getX(), myBoard.getCurrentPlayerPositions().get(0).getY() + direction.getY()));
        if (cellType == Board.CellType.UNBREAKABLE)
            return;

//        System.out.println("Current player(1) position: [x: " + myBoard.getCurrentPlayerPositions().get(0).getX() + ", y: " + myBoard.getCurrentPlayerPositions().get(0).getY() + "]");

        output.println(1);
        output.println(direction.getValue());
    }

    public void destroyRequest(Board.Direction direction){
        output.println(2);
        output.println(direction.getValue());
    }

    public static void main(String[] args) {
        Client client = new Client(HOST, PORT);
//        ClientHello hello = new ClientHello();
//        hello.getJb().addMouseListener(new MouseInputListener() {
//            @Override
//            public void mouseClicked(MouseEvent e) {
//                String host = hello.getJt().getText();
//                Client client = new Client(host, PORT);
//            }
//
//            @Override
//            public void mousePressed(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseReleased(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseEntered(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseExited(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseDragged(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseMoved(MouseEvent e) {
//
//            }
//        });
    }

}
