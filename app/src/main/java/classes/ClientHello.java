package classes;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Anvar on 29.12.15.
 */
public class ClientHello extends JFrame {
    private JLabel jl;
    private JTextField jt;

    public JButton getJb() {
        return jb;
    }

    public void setJb(JButton jb) {
        this.jb = jb;
    }

    private JButton jb;
    public JTextField getJt() {
        return jt;
    }
    public ClientHello() {
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("labyrinth");
        this.setLayout(new FlowLayout());
        this.setBounds(100, 100, 300, 300);
        jl = new JLabel("Enter server ip address: ");
        this.add(jl);
        jt = new JTextField(20);
        this.add(jt);
        jb = new JButton("OK");
        this.add(jb);
        this.setVisible(true);
    }
}
