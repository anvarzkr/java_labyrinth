package classes;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Anvar on 29.12.15.
 */
public class ServerHello extends JFrame {
    private JLabel jl;
    private JTextField jt;

    public JButton getJb() {
        return jb;
    }

    public void setJb(JButton jb) {
        this.jb = jb;
    }

    private JButton jb;
    public JTextField getJt() {
        return jt;
    }
    public ServerHello() {
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("labyrinth server");
        this.setLayout(new FlowLayout());
        this.setBounds(100, 100, 300, 300);
        jl = new JLabel("Server is running");
        this.add(jl);
        this.setVisible(true);
    }
}
