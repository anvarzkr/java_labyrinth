package classes;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by Anvar on 12.12.15.
 */
public class Server {

    public static final int PORT = 8901;

    private int gameSessionId = 0;

    public void start(int port){

        ServerSocket listener = null;

        try {
            listener = new ServerSocket(PORT);

            while (true) {
                Game game = new Game(gameSessionId++);

                while (game.getPlayers().size() != 2)
                    game.getPlayers().add(game.new Player(listener.accept()));

                if(game.start()){
                    System.out.println("Game started!");
                } else {
                    System.err.println("There some problems with starting the game!");
                    System.err.println("Not enough players!");
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (listener != null) {
                try {
                    listener.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        ServerHello serverHello = new ServerHello();
        new Server().start(PORT);
    }

}
