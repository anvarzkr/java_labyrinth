package classes;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Anvar on 12.12.15.
 */
public class Game {

    private ArrayList<Player> players;
    private Player currentPlayer;
    private Board board;
    private int gameSessionId;

    private Board.CellType[][] field;
    private ArrayList<Coordinates> currentPlayerPositions;

    public Game(int gameSessionId) {
        this.gameSessionId = gameSessionId;
        setPlayers(new ArrayList<Player>());

        field = new Board.CellType[Board.BOARD_VALID_SIZE + 2][Board.BOARD_VALID_SIZE + 2];

        currentPlayerPositions = new ArrayList<>();

        getRandomPlayerPositions();

        initializeField();
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }
    public ArrayList<Player> getPlayers() {
        return players;
    }
    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }


    private void initializeField(){
        if (field == null)
            return;

        Random random = new Random();

        boolean firstIteration = true;

        while (firstIteration || !checkForPlayersDestination()) {
            for (int i = 0; i < field.length; i++) {
                for (int j = 0; j < field[i].length; j++) {
                    if (i == 0 || i == field.length - 1 || j == 0 || j == field[i].length - 1) {
                        field[i][j] = Board.CellType.UNBREAKABLE;
                    } else {
                        int randomNumber = random.nextInt(4);
                        Board.CellType cellType;
                        if (randomNumber == 0) {
                            cellType = Board.CellType.UNBREAKABLE;
                        } else if (randomNumber == 1) {
                            cellType = Board.CellType.BREAKABLE;
                        } else {
                            cellType = Board.CellType.EMPTY;
                        }
                        field[i][j] = cellType;
                    }
                }
            }
            firstIteration = false;
        }
    }

    private boolean checkForPlayersDestination(){
        boolean[][] used = new boolean[field.length][field.length];

        dfs(used, currentPlayerPositions.get(0).getX(), currentPlayerPositions.get(0).getY());

        for (boolean[] br : used){
            for (boolean b : br){
                System.out.print(((b) ? 1 : 0) + " ");
            }
            System.out.println();
        }

        if (used[currentPlayerPositions.get(1).getX()][currentPlayerPositions.get(1).getX()]){
            return true;
        }

        return false;
    }

    private void dfs(boolean[][] used, int i, int j){
        used[i][j] = true;
        System.out.println(i + " " + j);

        if (i - 1 > 0 && !used[i - 1][j] && (field[i - 1][j].equals(Board.CellType.EMPTY) || field[i - 1][j].equals(Board.CellType.BREAKABLE))){
            dfs(used, i - 1, j);
        }
        if (i + 1 < used.length && !used[i + 1][j] && (field[i + 1][j].equals(Board.CellType.EMPTY) || field[i + 1][j].equals(Board.CellType.BREAKABLE))){
            dfs(used, i + 1, j);
        }
        if (j - 1 > 0 && !used[i][j - 1] && (field[i][j - 1].equals(Board.CellType.EMPTY) || field[i][j - 1].equals(Board.CellType.BREAKABLE))){
            dfs(used, i, j - 1);
        }
        if (j + 1 < used.length && !used[i][j + 1] && (field[i][j + 1].equals(Board.CellType.EMPTY) || field[i][j + 1].equals(Board.CellType.BREAKABLE))){
            dfs(used, i, j + 1);
        }
    }

    public boolean start(){
        if (getPlayers().size() == 2) {
            this.setCurrentPlayer(getPlayers().get(0));
            this.board = new Board(null);
            getPlayers().get(0).setOpponent(getPlayers().get(1));
            getPlayers().get(1).setOpponent(getPlayers().get(0));
            getPlayers().get(0).setPlayerId(0);
            getPlayers().get(1).setPlayerId(1);

            getPlayers().forEach((player) -> player.start());
            return true;
        }else{
            return false;
        }
    }

    public void getRandomPlayerPositions(){

        Random random = new Random();

        for (int i = 0; i < 2; i++) {

            int x = random.nextInt(field.length - 2) + 1;
            int y = random.nextInt(field.length - 2) + 1;

            currentPlayerPositions.add(i, new Coordinates(x, y));
            System.out.println("COORDINATES: " + x + " " + y);
        }

    }


    class Player extends Thread {

        private Socket socket;
        private PrintWriter output;
        private BufferedReader input;
        private Player opponent;
        private int playerId;

        public Player(Socket socket) {
            this.socket = socket;

            try {
                System.out.println("Connection with " + socket + " opened!");

                output = new PrintWriter(socket.getOutputStream(), true);
                input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            } catch (IOException e) {
                System.out.println("Player died: " + e);
                getPlayers().remove(this);
            }

        }

        public int getPlayerId() {
            return playerId;
        }
        public void setPlayerId(int playerId) {
            this.playerId = playerId;
        }
        public Player getOpponent() {
            return opponent;
        }
        public void setOpponent(Player opponent) {
            this.opponent = opponent;
        }

        @Override
        public void run() {
            System.out.println("Player started playing!");

            output.println(0); // STARTING THE GAME

            try {

                loop: while (true) {
                    System.out.println("Waiting for message from client..");
                    int action = Integer.parseInt(input.readLine());
                    System.out.println("action: " + action);

                    switch (action) {
                        case 1:
                            int directionValue = Integer.parseInt(input.readLine());
                            Board.Direction direction = Board.Direction.getDiretionFromValue(directionValue);

                            if (getCurrentPlayer().playerId != this.playerId)
                                break;

                            if (!checkValidMove(direction, this.playerId)) {

                                getPlayers().forEach((player) -> {
                                    player.output.println(2);
                                    player.output.println(direction.getValue());
                                    player.output.println(Board.CellType.UNDEFINED_WALL.getValue());

                                    if (player.playerId == this.playerId){
                                        player.output.println(0); // HIS TURN
                                    }else{
                                        player.output.println(1); // NOT HIS TURN
                                    }
                                });

                                output.println(4); // SHOW DIALOG
                                output.println(direction.getValue());

                                break;
                            }

                            movePlayer(this.playerId, direction);

                            setNextPlayerTurn();

                            System.out.println("Player moved!");
                            break;
                        case 2:
                            int destroyDirectionValue = Integer.parseInt(input.readLine());
                            Board.Direction destroyDirection = Board.Direction.getDiretionFromValue(destroyDirectionValue);

                            if (getCurrentPlayer().playerId != this.playerId)
                                break;

                            if (playerDestroyed(destroyDirection, this.playerId)){
                                getPlayers().forEach((player) -> {
                                    if (player.playerId == this.playerId){
                                        player.output.println(8); // HIS TURN
                                    }else{
                                        player.output.println(9); // NOT HIS TURN
                                    }
                                });
                                break loop;
                            }else{
                                if (field[currentPlayerPositions.get(this.playerId).getX() + destroyDirection.getX()][currentPlayerPositions.get(this.playerId).getY() + destroyDirection.getY()].equals(Board.CellType.BREAKABLE)){
                                    System.out.println("IT'S A BREAKABLE WALL");
                                    field[currentPlayerPositions.get(this.playerId).getX() + destroyDirection.getX()][currentPlayerPositions.get(this.playerId).getY() + destroyDirection.getY()] = Board.CellType.BREAKED_WALL;
                                }

                                getPlayers().forEach((player) -> {
                                    player.output.println(2);
                                    player.output.println(destroyDirection.getValue());
                                    player.output.println(field[currentPlayerPositions.get(this.playerId).getX() + destroyDirection.getX()][currentPlayerPositions.get(this.playerId).getY() + destroyDirection.getY()].getValue());
//                                player.output.println(Board.CellType.UNDEFINED.getValue());

                                    if (player.playerId == this.playerId){
                                        player.output.println(0); // HIS TURN
                                    }else{
                                        player.output.println(1); // NOT HIS TURN
                                    }
                                });

                                if (field[currentPlayerPositions.get(this.playerId).getX() + destroyDirection.getX()][currentPlayerPositions.get(this.playerId).getY() + destroyDirection.getY()].equals(Board.CellType.BREAKED_WALL)) {
                                    movePlayer(this.playerId, destroyDirection);
                                }
                            }

                            setNextPlayerTurn();

                            System.out.println("Something was destroyed... Or not..");
                            break;
                        case 4:

                            break;
                        default:
                            System.out.println("Unexpected action from server!");
                            break loop;
                    }
                }

            } catch (NullPointerException npe){
                npe.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                System.out.println("Connection with " + socket + " closed.");
                getPlayers().remove(this);
            }

        }
    }

    private void movePlayer(int playerId, Board.Direction direction){
        currentPlayerPositions.get(playerId).makeOffset(direction.getX(), direction.getY());

        getPlayers().forEach((player) -> {
            player.output.println(1);
            player.output.println(direction.getValue());
            player.output.println(field[currentPlayerPositions.get(playerId).getX()][currentPlayerPositions.get(playerId).getY()].getValue());

            if (player.playerId == playerId){
                player.output.println(0); // HIS TURN
            }else{
                player.output.println(1); // NOT HIS TURN
            }
        });
    }

    private void setNextPlayerTurn(){
        for (Player player : getPlayers()){
            if (currentPlayer != player) {
                setCurrentPlayer(player);
                break;
            }
        }
    }

    private boolean checkValidMove(Board.Direction direction, int playerId){
        System.out.println("CHECK FOR VALID MOVE:");
        System.out.println("X: " + (currentPlayerPositions.get(playerId).getX() + direction.getX()));
        System.out.println("Y: " + (currentPlayerPositions.get(playerId).getY() + direction.getY()));
        if (currentPlayerPositions.get(playerId).getX() + direction.getX() < 0 || currentPlayerPositions.get(playerId).getX() + direction.getX() >= field.length) {
            System.out.println("NOT VALID MOVE!");
            return false;
        }
        if (currentPlayerPositions.get(playerId).getY() + direction.getY() < 0 || currentPlayerPositions.get(playerId).getY() + direction.getY() >= field.length) {
            System.out.println("NOT VALID MOVE!");
            return false;
        }

        System.out.println("CELL TYPE: " + field[currentPlayerPositions.get(playerId).getX() + direction.getX()][currentPlayerPositions.get(playerId).getY() + direction.getY()].toString());

        if (field[currentPlayerPositions.get(playerId).getX() + direction.getX()][currentPlayerPositions.get(playerId).getY() + direction.getY()].equals(Board.CellType.EMPTY)
                || field[currentPlayerPositions.get(playerId).getX() + direction.getX()][currentPlayerPositions.get(playerId).getY() + direction.getY()].equals(Board.CellType.BREAKED_WALL)){
            System.out.println("VALID MOVE!");
            return true;
        }
        System.out.println("NOT VALID MOVE!");
        return false;
    }

    private boolean playerDestroyed(Board.Direction direction, int playerId){
        Coordinates enemyPlayerCoordinates = new Coordinates(currentPlayerPositions.get((playerId + 1) % 2).getX(), currentPlayerPositions.get((playerId + 1) % 2).getY());
        Coordinates destroyingPointCoordinates = new Coordinates(currentPlayerPositions.get(playerId).getX() + direction.getX(), currentPlayerPositions.get(playerId).getY() + direction.getY());

        if (enemyPlayerCoordinates.getX() == destroyingPointCoordinates.getX() && enemyPlayerCoordinates.getY() == destroyingPointCoordinates.getY()){
            return true;
        }

        return false;
    }

}
