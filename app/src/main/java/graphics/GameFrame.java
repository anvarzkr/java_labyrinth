package graphics;

import classes.Board;
import classes.Client;
import classes.Coordinates;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Anvar on 12.12.15.
 */
public class GameFrame extends JFrame {

    public static final int SQUARE_SIZE = 35;

    private BorderCell[][] myBorderCells;
    private BorderCell[][] enemyBorderCells;

    public GameFrame() {
        super("Game Frame");
        myBorderCells = new BorderCell[Board.BOARD_SIZE][Board.BOARD_SIZE];
        enemyBorderCells = new BorderCell[Board.BOARD_SIZE][Board.BOARD_SIZE];
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));

        for (int k = 0; k < 2; k++) {
            JPanel halfJPanel = new JPanel();
            halfJPanel.setLayout(new BoxLayout(halfJPanel, BoxLayout.Y_AXIS));
            getContentPane().add(halfJPanel);

            if (k == 0){
                halfJPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 10, Color.LIGHT_GRAY));
            }else{
                halfJPanel.setBorder(BorderFactory.createMatteBorder(0, 10, 0, 0, Color.LIGHT_GRAY));
            }

            for (int i = 0; i < Board.BOARD_SIZE; i++) {
                JPanel horizontalPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
                halfJPanel.add(horizontalPanel);
                for (int j = 0; j < Board.BOARD_SIZE; j++) {
                    if (k == 0) {
                        myBorderCells[j][i] = new BorderCell(SQUARE_SIZE, Color.LIGHT_GRAY, horizontalPanel);
                        myBorderCells[j][i].getCoordinates().setXY(i, j);
                        myBorderCells[j][i].setBackground(Color.WHITE);
                    }else{
                        enemyBorderCells[j][i] = new BorderCell(SQUARE_SIZE, Color.LIGHT_GRAY, horizontalPanel);
                        enemyBorderCells[j][i].getCoordinates().setXY(i, j);
                        enemyBorderCells[j][i].setBackground(Color.WHITE);
                    }
                }
            }
        }

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        mark(Board.BOARD_SIZE / 2, Board.BOARD_SIZE / 2, 0, Board.CellType.PLAYER);
        mark(Board.BOARD_SIZE / 2, Board.BOARD_SIZE / 2, 1, Board.CellType.PLAYER);
    }

    public void mark(Coordinates coordinates, int playerIndex, Board.CellType cellType){
        this.mark(coordinates.getX(), coordinates.getY(), playerIndex, cellType);
    }

    public void mark(int x, int y, int playerIndex, Board.CellType cellType){
        Color color = Color.WHITE;

        switch (cellType){
            case EMPTY:
                color = Color.LIGHT_GRAY;
                break;
            case BREAKABLE:
                color = Color.CYAN;
                break;
            case UNBREAKABLE:
                color = Color.BLACK;
                break;
            case PLAYER:
                color = Color.PINK;
                break;
            case UNDEFINED:
                color = Color.WHITE;
                break;
            case UNDEFINED_WALL:
                color = Color.YELLOW;
                break;
            case BREAKED_WALL:
                color = Color.CYAN;
            default:
                break;
        }

        BorderCell[][] borderCells = (playerIndex == 0) ? myBorderCells : enemyBorderCells;
        borderCells[x][y].setBackground(color);
    }

    public BorderCell[][] getBorderCells(int playerIndex) {
        return playerIndex == 0 ? myBorderCells : enemyBorderCells;
    }

    public static void main(String[] args) {
        new GameFrame();
    }

}

