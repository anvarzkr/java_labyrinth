package graphics;

import classes.Coordinates;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.peer.PopupMenuPeer;

/**
 * Created by Anvar on 12.12.15.
 */
public class BorderCell extends JPanel {

    private Coordinates coordinates;

    public BorderCell(int size, Color borderColor, JPanel parentJPanel) {
        super();
        this.setPreferredSize(new Dimension(size, size));
        this.setBorder(BorderFactory.createLineBorder(Color.lightGray));
        this.setCoordinates(new Coordinates(0, 0));
        parentJPanel.add(this);
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
}
