package graphics;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Anvar on 29.12.15.
 */
public class StartGame extends JFrame {
    private JLabel jl;

    public StartGame() {
//        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("Labyrinth");
        this.setLayout(new FlowLayout());
        this.setBounds(100, 100, 300, 300);

        jl = new JLabel("Waiting for the player to connect...");
        this.add(jl);

        this.setVisible(true);
    }
}